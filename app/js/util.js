//Public
var layers = [];
function createMap() {
    mapboxgl.accessToken = 'pk.eyJ1Ijoia3liZXJvcmciLCJhIjoiY2o0M3dlZTVkMGJ0ZDJxbnRpY2gya3hneiJ9.UE0tFHZ5S2XdSirRVV7XSw';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [24.763603, 59.433773], // starting position
        zoom: 3.5 // starting zoom
    });

    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl());

    return map;
}

function addLayer(label, dataSet, color) {
    var id = Math.random().toString(36).substring(5);
    map.addLayer({
        'id': id,
        'type': 'line',
        'source': {
            'type': 'geojson',
            'data': "./" + dataSet
        },
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': color,
            'line-width': 2
        }
    });
    layers.push(new Layer(id, label));
}

function addLayersToSwitcher(layers) {
    for (var i = 0; i < layers.length; i++) {
        var link = addLayerToSwitcher(layers[i]);
        $("#menu").append(link);
    }
    addOnClickHandlerForLayerSwitches();
}

function i18n() {
    var opts = {language: selectLanguage(), pathPrefix: "i18n"};
    $("[data-localize]").localize("strings", opts);

}

//Private

function addOnClickHandlerForLayerSwitches() {
    $("a.layerSwitch").on('click', function (e) {
        var clickedLayerName = $(this).data(layerId);
        e.preventDefault();
        e.stopPropagation();

        var visibility = map.getLayoutProperty(clickedLayerName, 'visibility');
        if (visibility === 'visible' || visibility === undefined) {
            map.setLayoutProperty (clickedLayerName, 'visibility', 'none');
            $(this).removeClass('active');
        } else {
            map.setLayoutProperty (clickedLayerName, 'visibility', 'visible');
            $(this).addClass('active');
        }

        var clickedLayer = $("#" + clickedLayerName);
        var visible = clickedLayer.is(":visible");
        if (visible) {
            clickedLayer.hide();
        } else {
            clickedLayer.show();
        }
    });
}

function addLayerToSwitcher(layer) {
    var link = document.createElement('a');
    $(link).attr('href', "#");
    $(link).addClass('active');
    $(link).addClass('layerSwitch');
    $(link).attr('data-' + layerId, layer.id);
    $(link).attr(dataLocalize, layer.label);

    return link;
}

function Layer(id, description) {
    this.id = id;
    this.label = description;
}

var layerId = 'layer-id';
var dataLocalize = 'data-localize';

function selectLanguage() {
    var req = new XMLHttpRequest();
    req.open('GET', document.location, false);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase();
    var lang = headerByName("X-Lang", headers).trim();
    if (lang === "ee" || lang === "ru" || lang === "en") {
        return lang;
    } else {
        return "ee";
    }
}

function headerByName(headerName, headers) {
    var headersArr = headers.match(/[^\r\n]+/g);
    for (var i = 0; i < headersArr.length - 1; i++) {
        var header = headersArr[i];
        var selectedHeaderName = header.split(":")[0];
        if (selectedHeaderName === headerName) {
            return header.split(":")[1];
        }
    }
    return "";
}
