var map;

function getGeoJson(pathToJson, color) {
    var geoJson = L.geoJson(null, {
        style: function (feature) {
            return {
                color: color,
                fill: false,
                opacity: 1,
                clickable: true
            };
        }
    });
    $.getJSON(pathToJson, function (data) {
        geoJson.addData(data);
    });
    return geoJson;
}

$(window).resize(function() {
    sizeLayerControl();
});

$("#about-btn").click(function() {
    $("#aboutModal").modal("show");
    $(".navbar-collapse.in").collapse("hide");
    return false;
});

$("#full-extent-btn").click(function() {
    $(".navbar-collapse.in").collapse("hide");
    return false;
});

$("#status-btn").click(function() {
    $('#sidebar').toggle();
    map.invalidateSize();
    return false;
});

$("#nav-btn").click(function() {
    $(".navbar-collapse").collapse("toggle");
    return false;
});

function sizeLayerControl() {
    $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

/* Basemap Layers */
var Esri_WorldTopoMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
});
var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});
var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
var Esri_WorldStreetMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
});

/* Trips */
var viimsi = getGeoJson('trips/viimsi-matka.geojson', 'DeepSkyBlue');
var narva = getGeoJson('trips/narva-matka.geojson', 'GreenYellow');
var pakri_offroad = getGeoJson('trips/paldiski-offroad.geojson', 'Brown');
var s_suomi = getGeoJson('trips/etela-suomi.geojson', 'Khaki');
var valga = getGeoJson('trips/valga-matka.geojson', 'PapayaWhip');
var turku = getGeoJson('trips/turusse-dimaga.geojson', 'DeepSkyBlue');
var lahti = getGeoJson('trips/suomen-lahti.geojson', 'Gold');
var elutee = getGeoJson('trips/elutee.geojson', 'Red');
var terve_eesti = getGeoJson('trips/terve-eesti.geojson', 'Gray');
var pm = getGeoJson('trips/pm.geojson', 'Blue');
var jlk = getGeoJson('trips/jimny-laheb-koju.geojson', 'Yellow');
var pokatushka = getGeoJson('trips/pokatushka.geojson', 'Maroon');
var joulu17 = getGeoJson('trips/joulumatka.geojson', 'DarkOrchid');
var koporska = getGeoJson('trips/koporska.geojson', 'YellowGreen');
var jls = getGeoJson('trips/jimny-laheb-saksamaale.geojson', 'Green');
var rannamatka = getGeoJson('trips/rannamatka.geojson', 'BurlyWood');
var kolm_karjalat = getGeoJson('trips/3karjalat.geojson', 'LightGreen');
var synnipaeva17 = getGeoJson('trips/synnipaiva2017.geojson', '#123456');

map = L.map("map", {
    zoom: 4,
    center: [59.422451,24.789511],
    layers: [Esri_WorldStreetMap, pm, jls, jlk, synnipaeva17],
    zoomControl: false,
    attributionControl: false
});

/* Attribution control */
function updateAttribution(e) {
    $.each(map._layers, function(index, layer) {
        if (layer.getAttribution) {
            $("#attribution").html((layer.getAttribution()));
        }
    });
}
map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

var attributionControl = L.control({
    position: "bottomright"
});
attributionControl.onAdd = function (map) {
    var div = L.DomUtil.create("div", "leaflet-control-attribution");
    div.innerHTML = "<span class='hidden-xs'>Originally Developed by <a href='http://bryanmcbride.com'>bryanmcbride.com</a> | </span><a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Attribution</a>";
    return div;
};
map.addControl(attributionControl);

var zoomControl = L.control.zoom({
    position: "bottomright"
}).addTo(map);

var scale = L.control.scale({
    position: "bottomleft",
    imperial: false
}).addTo(map);

/* Larger screens get expanded layer control and visible sidebar */
var isCollapsed = document.body.clientWidth <= 767;

var baseLayers = {
    "Esri" : Esri_WorldTopoMap,
    "Satelliit": Esri_WorldImagery,
    "OSM": OpenStreetMap_Mapnik,
    "Esri Maailm": Esri_WorldStreetMap
};

var groupedOverlays = {
    "Teekond": {
        "2016 Veb - Viimsi matk": viimsi,
        "2016 Apr - Testime rehvid": narva,
        "2016 Apr - Pakri Offroad": pakri_offroad,
        "2016 Apr - Lõuna Soome": s_suomi,
        "2016 Apr - Valga-Valka matk": valga,
        "2016 May - Turusse Dimaga": turku,
        "2016 May - Soome lahde ümber": lahti,
        "2016 Jun - Elutee": elutee,
        "2016 Jun - Terve Eesti ühel päeval": terve_eesti,
        "2016 Jun - Matk Euroopa põhjale": pm,
        "2016 Okt - Jimny läheb koju": jlk,
        "2016 Nov - Pokatushka": pokatushka,
        "2016 Talv - Jõulu Seiklus": joulu17,
        "2017 Mar - Koporje laht": koporska,
        "2017 Mar - Jimny läheb Saksamaale": jls,
        "2017 May - Rannamatk": rannamatka,
        "2017 May - 3 Karjalat": kolm_karjalat,
        "2017 Jun - Sünnipäevamatk": synnipaeva17
    }
};

//adding layerControl
L.control.groupedLayers(baseLayers, groupedOverlays, {
    collapsed: isCollapsed = true
}).addTo(map);

$( document ).ready(function() {
    //i18n
    i18n();
});


$(document).one("ajaxStop", function () {
    sizeLayerControl();
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
    L.DomEvent
        .disableClickPropagation(container)
        .disableScrollPropagation(container);
} else {
    L.DomEvent.disableClickPropagation(container);
}
