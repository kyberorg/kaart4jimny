FROM nginx:stable
COPY site.template /tmp/site.template
COPY app /srv
ENV EE_HOST=ee RU_HOST=ru EN_HOST=en
CMD /bin/bash -c "envsubst < /tmp/site.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"
EXPOSE 8080
